# Covert Channel

## Meta

Auteur 	: Fontaine Pierre

Date 	: Octobre 2019

## Introduction

L'objectif ici est de montrer qu'il est possible de mettre en place un moyen de communiquer entre deux programmes sans utiliser les protocoles de communications standards. Par moyens de communication standard on entend ici : socket, IPC, ...

## Idée

Nous avons vu qu'il était possible de monitorer l'usage des librairies partagées depuis le *user space* en instrumentant le cache.

Sur une machine Linux, un simple coup d'oeil au répertoire `/lib64/` nous donne un aperçu de toutes les librairies partagées qui peuvent être utilisées par les processus tournant sur notre machine. Dans notre cas, il nous faudra une librairie partagée qui soit la moins utilisée et assez grande. En effet, plus une librairie est utilisée, plus cela créera de bruit dans notre traitement et donc pourra mener à de faux positifs. Dans un deuxième temps elle devra être assez grande car nous utiliserons différents *offsets* qui doivent être corrects et nous verrons que cela pourra être un discriminant pour l'efficacité de notre communication. 

Nous nous plaçons dans un contexte où le serveur reste en écoute constante et ne peut pas acquitter une transaction.

## Builder le projet

Ce mini projet étant assez conséquent, le build s'effectue via `CMake`.

Si vous n'êtes pas familiarisé avec cet outil, deux choix son possible. Dans le premier cas, vous disposez d'un IDE (ie : CLion) qui *buildera* le projet dès lors qu'il trouvera le `CMakeLists.txt`, vous pourrez ensuite `run` les cibles qui sont `server` et `client`. Dans le second cas, vous êtes un adepte du terminal et vous effectuez les commandes suivantes :

```bash
cd src/
cmake -S . -B build/
cd ./build/client
make
cd ../server/
make
```

Notez qu'il est possible que vous ayez à installer un paquet ou à mettre à jour CMake, mais de part son utilisation très répandue il est fort probable que l'utilitaire soit déjà sur votre machine Linux !

## Flush + Reload et Offsets

Comme dit précédemment, nous allons nous basé sur une librairie partagée spécifique et nous allons faire des chargements à différents offset pour différents messages.

Partons du principe que deux offsets sont correctes s'il lors du chargement ils appellent deux pages de caches différentes. Pour connaître la taille de page sur une machine Linux, il suffit d’exécuter la commande suivante dans un terminal.

```bash
[pfontaine@precision ~]$ getconf PAGESIZE 
4096
```

La valeur retournée est en octets.

![](./memory_map.svg)

En hexadécimal, cet offset vaudra `0x1000`.

## Communications et symboles

Ce concept s'applique à tout support permettant l'envoi de message et nous verrons dans la suite de ce document comment améliorer une communication.

Du moins pour notre PoC de base, il faut comprendre que 5 symboles seront employés afin d'envoyer des données au format binaires et à délimiter les trames de communications. 

Ces symboles correspondront aux offsets par rapport à la base de notre librairie partagée, nous noterons alors que si chaque offset doit être espacé de 4096 octets nous devrons avoir une librairie dont la taille est supérieur à 4096 fois le nombre de symboles choisis pour la communication.
$$
card(s) \le sizeof(lib)
$$


## Modélisation

Nous allons tenter de mettre en place un protocole, proche de l'UDP afin de faire communiquer facilement un client et un serveur. Nous tenterons de mettre en place une architecture modulable. Pour cela nous utiliserons le langage `C++` qui nous offre les avantages du bas niveau, de l'orienté objet, ainsi qu'un large éventail de librairies.

Dans cette section nous allons détailler l'architecture ainsi que les différents diagrammes d'états et de séquence afin de détailler le fonctionnement de notre architecture et protocole. Cela permettra de présenter les concepts mis en place sans entrer dans le détails des classes et méthodes qui sont nombreuses.

### Diagramme de séquence

Dans les diagramme de séquence présentés ci dessous, nous allons mettre en avant le protocole et ses symboles associés qui va permettre à un client d'envoyer un message à un serveur en écoute.

Le premier diagramme présente le protocole avec un haut degré d'abstraction, le second met en avant l'utilisation du cache dans la communication.

![](./qeq_2.svg)

![](./seq_1.svg)

### Diagramme de classe

Nous allons devoir mettre en place une architecture qui supportera notre protocole. Cette architecture met évidemment en avant un `Client` ainsi qu'un `Serveur`.

![](./diag_uml_2.svg)

#### Packet

Notre classe `Packet` permet de représenter les données que nous souhaitons faire transiter par blocs. Ils sont définis par un identifiant, une longueur, une payload ainsi qu'un checksum.

L'identifiant sera utile au serveur en écoute, en effet le serveur recevra plusieurs fois le même paquet, si le checksum de ce paquet est bon il l'ajoutera à sa pile de paquet. En revanche si le checksum n'est pas bon, il attend de recevoir le prochain avec le même identifiant.

Cette classe permet aussi de faire le lien entre la couche `réseau` avec notre structure qui s'apparente à un datagramme de protocole TCP/UDP et la couche `physique`. L'objectif est d'avoir un moyen de passer d'un niveau à l'autre sans perte d'information. La figure ci dessous schématise le concept.

![](./datagramme.svg)

#### Prober

Une instance de `Prober` permet de se synchroniser sur une librairie. Elle embarque des fonctions écrites en assembleur permettant d'effectuer des opérations propres à nos attaques.

#### Serveur

L'instance de serveur permet de recevoir des données à l'état brut et de les transformer en données utiles.

##### receive

Notre méthode `receive` est basée sur une machine d'états. 

![](./state_mach.svg)

#### Client

Le client a un message à envoyer. Ce message va être segmenter en paquet de petite taille afin d'être transmis sur notre canal caché.

## Test et Performance

Pour effectuer notre test nous allons prendre un document qui ne nous posera pas de problèmes en terme d'encodage. Pour cela rien de plus simple, nous allons nous envoyer un morceau de RFC.

Ci dessous les données envoyées par le client.

```txt
   +---+
   | C |                       +------------+
   | e | <-------------------->| End entity |
   | r |       Operational     +------------+
   | t |       transactions          ^
   | i |      and management         |  Management
   | f |       transactions          |  transactions        PKI
   | i |                             |                     users
   | c |                             v
   | a | =======================  +--+------------+  ==============
   | t |                          ^               ^
   | e |                          |               |         PKI
   |   |                          v               |      management
   | & |                       +------+           |       entities
   |   | <---------------------|  RA  |<----+     |
   | C |  Publish certificate  +------+     |     |
   | R |                                    |     |
   | L |                                    |     |
   |   |                                    v     v
   | R |                                +------------+
   | e | <------------------------------|     CA     |
   | p |   Publish certificate          +------------+
   | o |   Publish CRL                     ^      ^
   | s |                                   |      |  Management
   | i |                +------------+     |      |  transactions
   | t | <--------------| CRL Issuer |<----+      |
   | o |   Publish CRL  +------------+            v
   | r |                                      +------+
   | y |                                      |  CA  |
   +---+                                      +------+

```



Ci dessous les données reçu par le serveur.

```txt
   +---+
   | C |                       +------------+
   | e | <-------------------->| End entity |
   | r |       Operational     +------------+
   | t |       transactions          ^
   | i |      and management         |  Management
   | f |       transactions          |  transactions        PKI
   | i |                             |                     users
   | c |                             v
   | a | =======================  +--+------------+  ==============
   | t |                          ^               ^
   | e |                          |               |         PKI
   |   |                          v               |      management
   | & |                       +------+           |       entities
   |   | <---------------------|  RA  |<----+     |
   | C |  Publish certificate  +------+     |     |
   | R |                                    |     |
   | L |                                    |     |
   |   |                                    v     v
   | R |                                +------------+
   | e | <------------------------------|     CA     |
   | p |   Publish certificate          +------------+
   | o |   Publish CRL                     ^      ^
   | s |                                   |      |  Management
   | i |                +------------+     |      |  transactions
   | t | <--------------| CRL Issuer |<----+      |
   | o |   Publish CRL  +------------+            v
   | r |                                      +------+
   | y |                                      |  CA  |
   +---+                                      +------+
```



En terme de débit, cette implémentation atteint les 6 octets par secondes en brute et 5 octets par secondes en utile.

## Amélioration en vue d'une V2 [non développé]

### Augmenter le nombre de symboles

Jusqu'ici nous pouvions seulement envoyer des messages bit par bit. Cependant un bon moyen d'augmenter le débit de façon efficace serait de s'inspirer des *Constellation Encoding*. Le principe est simple, augmenter le nombre de symboles, chaque symbole correspondant à une valeur.

Pour faire l'analogie, les symboles sont les offsets et les données associées seront `00`, `01`, `10`,`11`. Pour avoir un encodage complet il nous faut un nombre de symboles égale à une puissance de 2.

| S         | valeurs encodées       | Min,Max | Card(S) |
| --------- | ---------------------- | ------- | ------- |
| $2 = 2¹ $ | $$0,1$$                | $0,1$   | $2$     |
| $4 = 2²$  | $$00, 01, 10, 11$$     | $0,3$   | $4$     |
| $8 = 2³$  | $$000,001, ..., 111 $$ | $0,7$   | $8$     |



#### Améliorer notre machine à état

L'objectif cherché ici est le passage à l'échelle lorsque nous voulons ajouter plus d'états. Cette structure garde quelques similitude avec la v1, mais ici nous allons chercher à *factoriser* les transitions.

![](/home/pfontaine/Bureau/CSSE/SCA/cover_channel/state_advanced_fact.svg)



## Observations

Il est a noté que l'utilisation de cette implémentation fonctionne bien lorsque nous sommes branché sur secteur.

Sur la figure suivante nous observons que le serveur en écoute utilise 100% d'un cœur est que celui ci est affecté à intervalle régulier à un nouveau cœur logique.

![1572034715321](/home/pfontaine/.config/Typora/typora-user-images/1572034715321.png)

En revanche le client lui se fond dans la masse.



## Conclusion

Nous avons vu qu'il était possible de faire passer de l'information d'un processus à un autre sur une même machine en utilisant des techniques telles que le Flush + Reload. 

Pour cette version rendue le débit n'est pas élevé mais comme vu, il devrait être aisément possible de doubler celui ci.

Nous notons aussi qu'afin d'avoir une communication fiable il est nécessaire d'avoir un protocole robuste. Il est aussi possible de vérifier l'intégrité des données reçues via un checksum et n'accepter que les paquets corrects mais ceci réduira nettement le débit.

## Pour aller plus loin

- [ ] Finir la machine d'état via la librairie Boost
- [ ] Faire une fonction permettant le calibrage automatique du seuil de hit/miss
- [ ] Utilisation multithreadé sur le pattern producteur/consommateur avec gestion de la concurrent pour l'accès à la pile.
- [ ] Tester différentes approches de probing

## Références

[1] http://www.ieee802.org/11/Documents/DocumentArchives/1999_docs/90747A%20-%20Constellation%20Bit%20Encoding.pdf

[2] https://linux.die.net/man/2/getpagesize

