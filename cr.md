# Cover Channel

## Introduction

L'objectif ici est de montrer qu'il est possible de mettre en place un moyen de communiquer entre deux programmes sans utiliser les protocoles de communications standards.

## Idée

Nous avons vu qu'il était possible de monitorer l'usage des librairies partagées depuis le *user space* en instrumentant le cache.

Sur une machine Linux, un simple coup d'oeil au répertoire `/lib64/` nous donne un aperçu de toutes les librairies partagées qui peuvent être utilisées par les processus tournant sur notre machine. Dans notre cas, il nous faudra une librairie partagée qui soit la moins utilisée et assez grande. En effet, plus une librairie est utilisée, plus cela créera de bruit dans notre traitement et donc pourra mener à de faux positifs. Dans un deuxième elle devra être assez grande car nous utiliserons différents *offsets* qui doivent être corrects. 

Nous nous plaçons dans un contexte ou le serveur reste en écoute constante et ne peut pas acquitter une transaction.

## Modélisation

Nous allons tenter de mettre en place un protocole, proche de l'UDP afin de faire communiquer facilement un client et un serveur. Nous tenterons de mettre en place une architecture modulable et simple à mettre à niveau. Pour cela nous utiliserons le langage `C++` qui nous offre les avantages du bas niveau ainsi qu'un large éventail de librairies qui nous éviteront de ré inventer la roue.

Dans cette section nous allons détailler l'architecture ainsi que les différentes diagrammes d'états et de séquence afin de détailler le fonctionnement de notre architecture et protocole.

### Diagramme de séquence

Dans le diagramme ci dessous nous allons mettre en avant le protocole qui va permettre à un client d'envoyer un message à un serveur en écoute.

![](./seq_1.svg)

### Diagramme de classe

Nous allons devoir mettre en place une architecture qui supportera notre protocole. Cette architecture met évidemment en avant un `Client` ainsi qu'un `Serveur`.

![](./diag_uml_1.svg)

#### Packet

Notre classe `Packet` permet de représenter les données que nous souhaitons faire transiter par blocs. Ils sont définis par un identifiant, une longueur, une payload ainsi qu'un checksum.

L'identifiant sera utile au serveur en écoute, en effet le serveur recevra plusieurs fois le même paquet, si le checksum de ce paquet est bon il l'ajoutera à sa pile de paquet. En revanche si le checksum n'est pas bon, il attend de recevoir le prochain avec le même identifiant.

#### Prober

Une instance de `Prober` permet de se synchroniser sur une librairie.

#### Serveur

L'instance de serveur permet de recevoir des données à l'état brut et de les transformer en données utiles.

##### receive

Notre méthode `receive` est basée sur une machine d'état (Mealy). 

![](./state_mach.svg)

#### Client

