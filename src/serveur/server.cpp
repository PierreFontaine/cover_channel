//
// Created by pfontaine on 26/09/19.
//
/**
 * @todo Use class value to define seuil
 */

#include "server.h"

using namespace std;

/**
 * @brief Destructeur du serveur
 */
Server::~Server(){}

/**
 * @brief Constructeur du serveur
 */
Server::Server() {
	_seuil_pass_to_eop = 5;
	_seuil_ret_to_undef = 2;
}

/**
 * @brief Surcharge du constructeur par defaut
 */
Server::Server(const Server& s){
	_p = s._p;
	_seuil_ret_to_undef = s._seuil_ret_to_undef;
	_seuil_pass_to_eop = s._seuil_pass_to_eop;
	_nom = s._nom;
	_addr = s._addr;
	_func_base = s._func_base;
	_stack_Packet = s._stack_Packet;
	_p = s._p;
}

Server::Server(string nom, Prober::mode m, int timeout, void **addr, void **func_base){
	_nom = nom;
	_p = new Prober(m, timeout);
	_addr = *addr;
	_func_base = *func_base;

}

/**
 * @brief getter pour nom du serveur
 * @return
 */
string Server::getName() const {
	return _nom;
}

void* Server::getAddr() const {
	return _addr;
}

void* Server::getFuncBase() const {
	return _func_base;
}

/**
 * @brief
 */
void Server::receive() {
	vector<int> res;
	Server_SM sm;
	bool transaction = true;

	while(transaction) {
		ostringstream loader;
		int zero = _p->probe(_func_base);
		int one  = _p->probe(_func_base + 0x1000);
		int eop  = _p->probe(_func_base + 0x1000 + 0x1000);
		int eot  = _p->probe(_func_base + 0x1000 + 0x1000 + 0x1000);
		int eoc  = _p->probe(_func_base + 0x1000 + 0x1000 + 0x1000 + 0x1000);

		if(zero) {
			//cout << "hit on zero" << endl;
			sm.transition(Server_SM::e_zero);
		}
		if(one){
			//cout << "hit on one" << endl;
			sm.transition(Server_SM::e_one);
		}
		if(eop) {
			//cout << "hit on eop" << endl;
			sm.transition(Server_SM::e_eop);
		}
		if(eot) {
			//cout << "hit on eot" << endl;
			sm.transition(Server_SM::e_eot);
		}
		if(eoc) {
			//cout <<"hit on eoc" << endl;
			sm.transition(Server_SM::e_eoc);
		}

		if (sm.isPacketEnded()) {
			switch(sm.getBit()) {
				default:break; //should raise an exception
				case Server_SM::O_ONE:
					res.push_back(1);
					break;
				case Server_SM::O_ZERO:
					res.push_back(0);
					break;
				case Server_SM::O_EOT: {
					packet p;
					Packet *P_o;
					//cout << "ENTIRE PACKET RECEIVED !" << endl;
					//cout << "sizeof stack " << res.size() << endl;
					//cout << "size should be " << sizeof(struct packet_s) * 8 << endl;
					p = Packet::vectToPacket(res);
					P_o = new Packet(p);
					loader.clear();
					if (P_o->isPacketCorrect()) {
						//cout << "packet correct !" << endl;
						if(!packetAlreadyExist(P_o)){
							_stack_Packet.push_back(P_o);
						}
					} else {
						//cout << "incorrect packet !" << endl;
						_stack_Packet.push_back(P_o);
					}
					system("clear");
					cout << "printing raw data" << endl;
					printRawData();
					res.clear();
					break;
				}
				case Server_SM:: O_EOC:
					transaction = false;
					break;
				case Server_SM::O_UNDEF:
					cerr << "Comportement undef" << endl;
					break;
			}
			sm.setPacketEnded(false);
		}
	}
}

void Server::printRawData() {
	ostringstream out;
	for (Packet* p : _stack_Packet) {
		out << p->getData();
	}

	cout << out.str() << endl;
}

/**
 * @brief
 * @param p
 * @return
 */
bool Server::packetAlreadyExist(Packet *p) const {
	bool res = false;
	int i = 0;

	while (i < _stack_Packet.size() && !res) {
		if (_stack_Packet[i]->getId() == p->getId()){
			res = true;
		}
		i++;
	}
}

/**
 * @brief
 */
void Server::printStackPacket() {
	for (Packet *e : _stack_Packet) {
		cout << *e << endl;
	}
}


