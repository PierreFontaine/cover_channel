//
// Created by pfontaine on 17/10/2019.
//

#include "Server_SM.h"

/** STATE MACHINE **/

/**
 * @brief constructor of our state machine
 */
Server_SM::Server_SM() {
	_s = Server_SM::state(UNDEF);
}

/**
 * @brief Allow developper to get the actual state of the instance
 * of State Machine.
 * @return the actual state
 */
Server_SM::state Server_SM::getActualState() {
	return _s;
}

void Server_SM::setPacketEnded(bool b) {
	_p_ok = b;
}

/**
 * @brief
 * @param e an event
 */
void Server_SM::transition(Server_SM::event e) {
	/**
	cout << "[TRANSITION]" << endl;
	cout << "\t current state : " << _s << endl;
	cout << "\t counters: "<< endl;
	cout << "\t\t o : " << _o << endl;
	cout << "\t\t z : " << _z << endl;
	cout << "\t\t eop : " << _eop << endl;
	cout << "\t\t eot : " << _eot << endl;
	cout << "\t\t eoc : " << _eoc << endl;
	**/
	switch(_s) {
		default:break;
		case UNDEF:
			undef_handler(e);
			break;
		case ZERO:
			zero_handler(e);
			break;
		case ONE:
			one_handler(e);
			break;
		case EOT:
			eot_handler(e);
			break;
		case EOP:
			eop_handler(e);
			break;
		case EOC:
			eoc_handler(e);
			break;
	}
}

/**
 * @brief handler for state UNDEFINED
 * @param e an event
 */
void Server_SM::undef_handler(Server_SM::event e) {
	switch (e) {
		default:break;
		case e_zero:
			_s = Server_SM::ZERO;
			_z++;
			break;
		case e_one:
			_s = Server_SM::ONE;
			_o++;
			break;
		case e_eot:
			_s = Server_SM::EOT;
			_eot++;
			break;
		case e_eoc:
			_s = Server_SM::EOC;
			_eoc++;
		case e_eop:
			if (_eop > 2)
				resetCounters();
			break;
	}
}

/**
 * @brief Handler for state ZERO
 * @param e an event
 */
void Server_SM::zero_handler(Server_SM::event e) {
	switch (e) {
		default:break;
		case e_zero:
			if (_z > 3) {
				_s = Server_SM::EOP;
			} else {
				_z++;
			}
			break;
		case e_one:
			_o++;
			if (_o > 1) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eot:
			_eot++;
			if (_eot > 1) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eop:
			_eop++;
			if (_eop > 1) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
	}
}

/**
 * @brief Handler for state ONE
 * @param e an event
 */
void Server_SM::one_handler(Server_SM::event e) {
	switch (e) {
		default:break;
		case e_zero:
			_z++;
			if (_z > 1) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_one:
			_o++;
			if (_o > 3) {
				_s = Server_SM::EOP;
			}
			break;
		case e_eot:
			_eot++;
			if (_eot > 1) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eop:
			_eop++;
			if (_eop > 1) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
	}
}

/**
 * @brief Handler for state EOT
 * @param e an event
 */
void Server_SM::eot_handler(Server_SM::event e) {
	switch (e) {
		default:break;
		case e_zero:
			_z++;
			if (_z > 1) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_one:
			_o++;
			if (_o > 1) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eot:
			_eot++;
			if (_eot > 3) {
				_s = Server_SM::EOP;
			}
			break;
		case e_eop:
			_eop++;
			if (_eop > 1) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
	}
}

/**
 * @brief
 * @param e
 */
void Server_SM::eop_handler(Server_SM::event e) {
	switch (e) {
		default:break;
		case e_zero:
			_z++;
			if (_z > 2 && _eop > 2) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_one:
			_o++;
			if(_o > 2 && _eop > 2) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eot:
			_eot++;
			if (_eot > 2 && _eop > 2) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eoc:
			_eoc++;
			if (_eot > 2 && _eop > 2) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eop:
			_eop++;
			if (_eop == 2) {
				_p_ok = true;
			}
			if (_eop > 2) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
	}
}

/**
 * @brief
 * @param e
 */
void Server_SM::eoc_handler(Server_SM::event e) {
	switch (e) {
		default:
			break;
		case e_zero:
			_z++;
			if (_z > 2 && _eoc < 3) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_one:
			_o++;
			if (_o > 2 && _eoc < 3) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eot:
			_eot++;
			if (_eot > 1 && _eoc < 3) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eop:
			_eop++;
			if (_eop > 1 && _eoc < 3) {
				_s = Server_SM::UNDEF;
				resetCounters();
			}
			break;
		case e_eoc:
			_eoc++;
			if (_eoc > 4) {
				_s = Server_SM::EOP;
				_p_eoc = true;
			}
	}
}

/**
 * @brief reset all internals counters
 */
void Server_SM::resetCounters() {
	_o = 0;
	_z = 0;
	_eop = 0;
	_eot = 0;
	_p_ok = false;
	_eoc = 0;
	_p_eoc = false;
}

/**
 * @brief Allow developper to know if the communication is over
 * @return a boolean, true if the comm is ended
 */
bool Server_SM::isCommEnded() const {
	return _p_eoc;
}

/**
 * @brief this function allows developper to know if a bit has been sent
 * @return a boolean, true if packet is ended
 */
bool Server_SM::isPacketEnded() {
	return _p_ok;
}

/**
 * @brief Allows developper to know what kind of bit has been sended
 * or if it was an end of transmission
 * @return
 */
int Server_SM::getBit() {
	if (isPacketEnded()) {
		if (_o > _z && _o > _eot && _o >_eoc) {
			return Server_SM::O_ONE;
		} else if (_z > _eot && _z > _eoc) {
			return Server_SM::O_ZERO;
		} else if (_eoc > _eot) {
			return Server_SM::O_EOC;
		} else {
			return Server_SM::O_EOT;
		}
	} else {
		return Server_SM::O_UNDEF; //Should raise an exception
	}
}