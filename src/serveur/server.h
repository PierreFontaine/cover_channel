//
// Created by pfontaine on 26/09/19.
//

#ifndef SRC_SERVER_H
#define SRC_SERVER_H

#include <iostream>
#include <string>
#include <stack>
#include <vector>
#include "prober.h"
#include "packet.h"
#include "Server_SM.h"

using namespace std;

/**
 * @brief A class for implementing a server which will listen on a specific function
 * and process packets
 * @todo delete space memory for each Packet stored in _stack_Packet
 */
class Server{
	private:
		int _seuil_ret_to_undef;
		int _seuil_pass_to_eop;
		std::string _nom;
		void* _addr;
		void* _func_base;
		vector<Packet*> _stack_Packet;
		Prober *_p;
	public:
		Server();
		Server(const Server&);
		Server(string nom, Prober::mode m, int timeout, void **addr, void **func_base);
		~Server();
		void receive();
		std::string getName() const;
		void* getAddr() const;
		void* getFuncBase() const;

		void printStackPacket();
		void printRawData();

	private:
		bool packetAlreadyExist(Packet *p) const;
};

inline ostream &operator <<(ostream &out, const Server& s1) {
	out << "Server information\n" <<
		"[-] nom   :      " << s1.getName() << "\n" <<
		"[-] _addr :      " << s1.getAddr() << "\n" <<
		"[-] _func_base : " << s1.getFuncBase() << endl;

	return out;
}

#endif //SRC_SERVER_H
