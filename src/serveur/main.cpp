#include <iostream>
#include "server.h"
#include <dlfcn.h>

#define LIBCURL_PATH "/lib64/libxenstat.so.0.0"
#define BASE_FUNC "xenstat_tmem_succ_pers_puts"

using namespace std;

int main() {
	char *dl_error = nullptr;
	void *library;
	void *base_func;
	Server *s = nullptr;

	library = dlopen(LIBCURL_PATH, RTLD_NOW);
	if (!library) {
		Prober::error("dlopen failed: %s", dl_error);
	}
	base_func = dlsym(library, BASE_FUNC);
	if ((dl_error = dlerror()) != nullptr)  {
		Prober::error("error in dlsym : %s",dl_error);
	}

	Prober::info("LIB is at      %p",library);
	Prober::info("CBI is at      %p",base_func);

	s = new Server("xen", Prober::FLUSH_RELOAD, 200, &library, &base_func);

	cout << "[!] Serveur " << s->getName() << " running ..." << endl;

	cout << *s << endl;
	cout << "[+] Mise en écoute " << endl;
	s->receive();
	cout << "[+] Fin de transmission " << endl;
	cout << "[+] Affichage des paquets " << endl;
	s->printStackPacket();
	cout << "------------------------" << endl;
	s->printRawData();

	return 0;
}