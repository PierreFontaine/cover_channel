//
// Created by pfontaine on 17/10/2019.
//

#ifndef SRC_SERVER_SM_H
#define SRC_SERVER_SM_H

/**
 * @brief A class implementing a State Machine
 * easily modulable by adding/removing states and
 * handlers.
 */
class Server_SM{
public:
	enum event{e_zero, e_one, e_eot, e_eop, e_eoc};
	enum out{O_ZERO, O_ONE, O_EOT, O_EOC, O_UNDEF};
private:
	enum state{UNDEF, ZERO, ONE, EOT, EOP, EOC};

	state _s;
	int _o = 0;
	int _z = 0;
	int _eop = 0;
	int _eot = 0;
	int _eoc = 0;
	bool _p_ok = false;
	bool _p_eoc = false;

public:
	Server_SM();
	void transition(Server_SM::event e);

	state getActualState();
	bool isPacketEnded();
	int getBit();
	bool isCommEnded() const;

	void setPacketEnded(bool b);
private:
	void undef_handler(event e);
	void zero_handler(event e);
	void one_handler(event e);
	void eot_handler(event e);
	void eop_handler(event e);
	void eoc_handler(event e);

	void resetCounters();
};

#endif //SRC_SERVER_SM_H
