//
// Created by pfontaine on 21/10/2019.
//

#include "Server_SM_Advanced.h"

/** STARTING POINT **/
template<class Event, class FSM>
void server_::on_exit(const Event &, FSM &) {
	cout << "Quitting Server" << endl;
}

template<class Event, class FSM>
void server_::on_entry(const Event &, FSM &) {
	cout << "Hello Server" << endl;
}

/** GENERAL **/

template<class Event, class FSM>
void server_::IDLE::on_entry(const Event &, FSM &) {

}

/** COUNTER COMPOSITE STATE **/

	/** GUARDS **/

	bool server_::COUNTER_::eoc_seuil(e_eoc const& evt) {
		return true;
	}

	bool server_::COUNTER_::eot_seuil(e_eot const& evt) {
		return true;
	}

	/** EOC **/

	template<class Event, class FSM>
	void server_::COUNTER_::EOC::on_entry(const Event &, FSM &) {

	}

	template<class Event, class FSM>
	void server_::COUNTER_::EOC::on_do(const Event &, FSM &) {
		cout << "Hello Server" << endl;
	}

	/** OO **/

	template<class Event, class FSM>
	void server_::COUNTER_::OO::on_entry(const Event &, FSM &) {

	}

	template<class Event, class FSM>
	void server_::COUNTER_::OO::on_do(const Event &, FSM &) {
		cout << "Hello Server" << endl;
	}

	/** OZ **/

	template<class Event, class FSM>
	void server_::COUNTER_::OZ::on_entry(const Event &, FSM &) {

	}

	template<class Event, class FSM>
	void server_::COUNTER_::OZ::on_do(const Event &, FSM &) {
		cout << "Hello Server" << endl;
	}

	/** ZO **/

	template<class Event, class FSM>
	void server_::COUNTER_::ZO::on_entry(const Event &, FSM &) {

	}

	template<class Event, class FSM>
	void server_::COUNTER_::ZO::on_do(const Event &, FSM &) {
		cout << "Hello Server" << endl;
	}

	/** ZZ **/

	template<class Event, class FSM>
	void server_::COUNTER_::ZZ::on_entry(const Event &, FSM &) {

	}

	template<class Event, class FSM>
	void server_::COUNTER_::ZZ::on_do(const Event &, FSM &) {
		cout << "Hello Server" << endl;
	}

	/** EOT **/

	template<class Event, class FSM>
	void server_::COUNTER_::EOT::on_entry(const Event &, FSM &) {

	}

	template<class Event, class FSM>
	void server_::COUNTER_::EOT::on_do(const Event &, FSM &) {
		cout << "Hello Server" << endl;
	}



