//
// Created by pfontaine on 21/10/2019.
//

#ifndef SRC_SERVER_SM_ADVANCED_H
#define SRC_SERVER_SM_ADVANCED_H

#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/state_machine_def.hpp>
#include <vector>
#include <iostream>

/**
 * @brief In this part we're trying to go further using the boost library
 * since our first version is working, this part aim to improve performance
 * but is still a test
 * @see https://www.boost.org/doc/libs/1_64_0/libs/msm/doc/HTML/examples/SimpleTutorial.cpp
 */

namespace msm = boost::msm;
namespace mpl = boost::mpl;

using namespace std;


/**
 * @brief Define Event as struct
 */

struct e_eoc        {};
struct e_oo         {};
struct e_oz         {};
struct e_zo         {};
struct e_zz         {};
struct e_eot        {};
struct e_sumOthers  {};

struct server_ : public msm::front::state_machine_def<server_> {
	/**
	 * @brief Defining entry point
	 */
	template <class Event, class FSM> void on_entry(Event const&, FSM&);
	template <class Event, class FSM> void on_exit(Event const&, FSM&);

	struct IDLE : public msm::front::state<> {
		template <class Event, class FSM> void on_entry(Event const&, FSM&);
		template <class Event, class FSM> void on_exit(Event const&, FSM&);
	};

	struct EOP : public msm::front::state<> {

	};

	struct COUNTER_ : public msm::front::state_machine_def<COUNTER_> {
		template <class Event, class FSM> void on_entry(Event const&, FSM&);
		template <class Event, class FSM> void on_exit(Event const&, FSM&);

		struct EOC : public msm::front::state<> {
			template <class Event, class FSM> void on_entry(Event const&, FSM&);
			template <class Event, class FSM> void on_do(Event const&, FSM&);
		};

		struct OO : public msm::front::state<> {
			template <class Event, class FSM> void on_entry(Event const&, FSM&);
			template <class Event, class FSM> void on_do(Event const&, FSM&);
		};

		struct OZ : public msm::front::state<> {
			template <class Event, class FSM> void on_entry(Event const&, FSM&);
			template <class Event, class FSM> void on_do(Event const&, FSM&);
		};

		struct ZO : public msm::front::state<> {
			template <class Event, class FSM> void on_entry(Event const&, FSM&);
			template <class Event, class FSM> void on_do(Event const&, FSM&);
		};

		struct ZZ : public msm::front::state<> {
			template <class Event, class FSM> void on_entry(Event const&, FSM&);
			template <class Event, class FSM> void on_do(Event const&, FSM&);
		};

		struct EOT : public msm::front::state<> {
			template <class Event, class FSM> void on_entry(Event const&, FSM&);
			template <class Event, class FSM> void on_do(Event const&, FSM&);
		};

		/** DEFINING TRANSITIONS FUNCTIONS **/

		void start_eop(e_eot const&) {cout << "server::start eop";}
		void start_eop(e_zz const&) {cout << "server::start eop";}
		void start_eop(e_zo const&) {cout << "server::start eop";}
		void start_eop(e_oz const&) {cout << "server::start eop";}
		void start_eop(e_oo const&) {cout << "server::start eop";}
		void start_eop(e_eoc const&) {cout << "server::start eop";}

		/** DEFINING GUARDS **/

		bool eoc_seuil(e_eoc const& evt);
		bool eot_seuil(e_eot const& evt);
		bool zz_seuil(e_zz const&);
		bool zo_seuil(e_zo const&);
		bool oz_seuil(e_oz const&);
		bool oo_seuil(e_oo const&);


		typedef COUNTER_ c;

		/** DEFINING TRANSITION TABLE **/

		struct transition_table : mpl::vector<
				row <EOT, e_eot, EOP, &c::start_eop, &c::eot_seuil>,
				row <ZZ, e_zz, EOP, &c::start_eop, &c::zz_seuil>,
				row <ZO, e_zo, EOP, &c::start_eop, &c::zo_seuil>,
				row <OZ, e_oz, EOP, &c::start_eop, &c::oz_seuil>,
				row <OO, e_oo, EOP, &c::start_eop, &c::oo_seuil>,
				row <EOC, e_eoc, EOP, &c::start_eop, &c::eoc_seuil>
				> {};
	};

	/** DEFINING TRANSITION FUNCTIONS **/

	void start_eoc(e_eoc const&) {cout << "server::start eoc";}
	void start_oo(e_oo const&) {cout << "server::start oo";}
	void start_oz(e_oz const&) {cout << "server::start oz";}
	void start_zo(e_zo const&) {cout << "server::start zo";}
	void start_zz(e_zz const&) {cout << "server::start zz";}
	void start_eot(e_eot const&) {cout << "server::start eot";}
	void resetCounters(e_sumOthers const&);

	/** DEFINING TRANSITION TABLE **/

	typedef server_ s;

	struct transition_table : mpl::vector<
			//    +-----+------+--------------+---------------+
			a_row < IDLE, e_eoc, COUNTER_::EOC, &s::start_eoc>,
            a_row < IDLE, e_oo , COUNTER_::OO, &s::start_oo>,
            a_row < IDLE, e_oz , COUNTER_::OZ, &s::start_oz>,
            a_row < IDLE, e_zo , COUNTER_::ZO, &s::start_zo>,
            a_row < IDLE, e_zz , COUNTER_::ZZ, &s::start_zz>,
            a_row < IDLE, e_eot , COUNTER_::EOT, &s::start_eot>,
            a_row   < COUNTER_, e_sumOthers, IDLE, &s::resetCounters>
			> {};
};

#endif //SRC_SERVER_SM_ADVANCED_H
