//
// Created by pfontaine on 09/10/2019.
//

#ifndef SRC_CLIENT_H
#define SRC_CLIENT_H

#include <iostream>
#include <prober.h>
#include <packet.h>
#include <vector>
#include <cstring>
#include <bitset>
#include "cstdio"

using namespace std;

class Client{
	void *_addr;
	void *_func_base;
	string _name;
	Prober::mode _m;
	int _timeout;
	vector<Packet> _stack_packet;
	Prober *_p;
	void *_addr_zero;
	void *_addr_one;
	void *_addr_eop;
	void *_addr_eot;
	void *_addr_eoc;

	public:
		Client(string name, Prober::mode m, int timeout, void **addr, void **func_base);

		/** GETTER **/
		void* getAddr() const;
		void* getFuncBase() const;
		string getName() const;
		vector<Packet> getStackPacket();

		/** SETTER **/
		void setAddr(void *addr);
		void setFuncAddr(void *addr);
		void setName(string s);

		/** MISC **/

		void charToPacket(char* buffer);
		void send();
		void sendPacket(char* serPacket, bool isLast);
		void sendBit(int b);
};

inline ostream &operator <<(ostream &out, const Client& c1) {
	out << "Client information\n" <<
		"[-] name  :      " << c1.getName() << "\n" <<
		"[-] _addr :      " << c1.getAddr() << "\n" <<
		"[-] _func_base : " << c1.getFuncBase() << endl;

	return out;
}

#endif //SRC_CLIENT_H
