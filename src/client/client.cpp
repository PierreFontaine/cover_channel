//
// Created by pfontaine on 09/10/2019.
//

#include "client.h"

using namespace std;

/**
 * @brief
 * @param name
 * @param m
 * @param timeout
 * @param addr
 * @param func_base
 */
Client::Client(string name, Prober::mode m, int timeout, void **addr, void **func_base) {
	_name = name;
	_m = m;
	_timeout = timeout;

	_addr = *addr;
	_func_base = *func_base;

	_p = new Prober(_m, timeout);

	_addr_zero = _func_base;
	_addr_one  = _func_base + 0x1000;
	_addr_eop  = _func_base + 0x1000 + 0x1000;
	_addr_eot  = _func_base + 0x1000 + 0x1000 + 0x1000;
	_addr_eoc  = _func_base + 0x1000 + 0x1000 + 0x1000 + 0x1000;
}

/**
 * @brief transform un buffer of an undefined size into packets
 * in order to feed our _stack_packet
 * @param buffer
 */
void Client::charToPacket(char *buffer) {
	string s(buffer);
	int len = s.size();

	if  (len > 0) {
		int nbChunks = len/64 + 1;

		for (int i = 0; i < nbChunks; ++i) {
			struct packet_s p{};
			string chunk_s;
			const char *chunk_c;
			Packet *p_o = nullptr;

			chunk_s = s.substr(i* 64, 64);
			chunk_c = chunk_s.c_str();
			p._id = i;
			p._length = chunk_s.size();
			memcpy(&p._data, chunk_c, 64);

			p_o = new Packet(p);
			p_o->calculChecksum();
			p = p_o->getPacket();
			delete p_o;
			Packet p_to_store(p);
			_stack_packet.push_back(p_to_store);
		}
	}
}

void Client::sendBit(int b) {
	if (b == 0x1) {
		cout << "\t\t\t [-] Probing a one" << endl;
		cout << "[-] _addr_zero : " << _addr_one << endl;
		for (int i = 0; i < 30; ++i) {
			_p->probe(_addr_one);
			usleep(_timeout);
		}
	} else if (b == 0x0) {
		cout << "\t\t\t [-] Probing a zero" << endl;
		cout << "[-] _addr_zero : " << _addr_zero << endl;
		for (int i = 0; i < 30; ++i) {
			_p->probe(_addr_zero);
			usleep(_timeout);
		}
	} else {
		cerr << "error from sendBit()" << endl;
	}
	for (int i = 0; i < 30; ++i) {
		_p->probe(_addr_eop);
		usleep(_timeout);
	}
}

/**
 * @brief Send a serialized packet
 * @param serPacket
 */
void Client::sendPacket(char *serPacket, bool isLast) {
	cout << "\t[@] Function sendPacket()" << endl;

	for (unsigned long i = 0; i < sizeof(struct packet_s); i++) {
		cout << "\t\t[-] sending byte[" << i <<"] " << endl;
		for (unsigned  int j = 0; j < 8; j++) {
			cout << "\t\t\t[-] sending bit[" << j << "] from byte[" << i << "]" << endl;
			printf("%#04x\n", serPacket[i]);
			sendBit(serPacket[i] >> j & 0b00000001);
			usleep(100);
		}
	}
	for (int i = 0; i < 30; ++i) {
		_p->probe(_addr_eot);
		usleep(_timeout);
	}
	for (int j = 0; j < 30; ++j) {
		_p->probe(_addr_eop);
		usleep(_timeout);
	}
	if (isLast) {
		for (int j = 0; j < 30; ++j) {
			cout << "\t\t\t [-] Probing a EOC" << endl;
			_p->probe(_addr_eoc);
			usleep(_timeout);
		}
		for (int j = 0; j < 30; ++j) {
			_p->probe(_addr_eop);
			usleep(_timeout);
		}
	}
}

/**
 * @brief This function will send all packets that are in the
 * stack using sendPacket() method
 * @param buffer
 */
void Client::send() {
	cout << "[@] Function send()" << endl;
	for (int i = 0; i < _stack_packet.size(); i++) {
		bool isLast = false;
		Packet p = _stack_packet[i];

		isLast = (i == (_stack_packet.size() - 1));
		char* serPacket = new char[sizeof(struct packet_s)];
		p.serializePacket(serPacket);
		cout << "print instance containing packet" << endl;
		cout << p << endl;
		cout << "printing serialized packet to send" << endl;
		cout << serPacket << endl;
		struct packet_s p_r{};
		memcpy(&p_r, serPacket, sizeof(struct packet_s));
		Packet P_r(p_r);
		cout << "testing if serialization does work" << endl;
		cout << p_r << endl;

		for (int j = 0; j < 1; j++) {
			cout << "\t[-] Sending packet " << i << endl;
			sendPacket(serPacket, isLast);
		}
	}
}

/**
 * @brief
 * @return
 */
void *Client::getAddr() const {
	return _addr;
}

/**
 * @brief
 * @return
 */
void *Client::getFuncBase() const {
	return _func_base;
}

/**
 * @brief
 * @return
 */
string Client::getName() const {
	return _name;
}

/**
 * @brief
 * @param addr
 */
void Client::setAddr(void *addr) {
	_addr = addr;
}

/**
 * @brief
 * @param addr
 */
void Client::setFuncAddr(void *addr) {
	_func_base = addr;
}

/**
 * @brief
 * @param s
 */
void Client::setName(string s) {
	_name = s;
}

vector<Packet> Client::getStackPacket() {
	return _stack_packet;
}
