//
// Created by pfontaine on 14/10/2019.
//

#include "client.h"
#include <cstring>
#include <chrono>

#define LIBCURL_PATH "/lib64/libxenstat.so.0.0"
#define BASE_FUNC "xenstat_tmem_succ_pers_puts"

using namespace std;

int main (int argc, char* argv[]) {
	Client *c = nullptr;
	char *dl_error = nullptr;
	void *library;
	void *base_func;

	char* buffer = new char[2048];

	cout << "création d'un message" << endl;

	string s = "   +---+\n"
	           "   | C |                       +------------+\n"
	           "   | e | <-------------------->| End entity |\n"
	           "   | r |       Operational     +------------+\n"
	           "   | t |       transactions          ^\n"
	           "   | i |      and management         |  Management\n"
	           "   | f |       transactions          |  transactions        PKI\n"
	           "   | i |                             |                     users\n"
	           "   | c |                             v\n"
	           "   | a | =======================  +--+------------+  ==============\n"
	           "   | t |                          ^               ^\n"
	           "   | e |                          |               |         PKI\n"
	           "   |   |                          v               |      management\n"
	           "   | & |                       +------+           |       entities\n"
	           "   |   | <---------------------|  RA  |<----+     |\n"
	           "   | C |  Publish certificate  +------+     |     |\n"
	           "   | R |                                    |     |\n"
	           "   | L |                                    |     |\n"
	           "   |   |                                    v     v\n"
	           "   | R |                                +------------+\n"
	           "   | e | <------------------------------|     CA     |\n"
	           "   | p |   Publish certificate          +------------+\n"
	           "   | o |   Publish CRL                     ^      ^\n"
	           "   | s |                                   |      |  Management\n"
	           "   | i |                +------------+     |      |  transactions\n"
	           "   | t | <--------------| CRL Issuer |<----+      |\n"
	           "   | o |   Publish CRL  +------------+            v\n"
	           "   | r |                                      +------+\n"
	           "   | y |                                      |  CA  |\n"
	           "   +---+                                      +------+";

	cout << "copie du message dans le buffer" << endl;
	memcpy(buffer, s.c_str() , strlen(s.c_str())+1);

	library = dlopen(LIBCURL_PATH, RTLD_NOW);

	if (!library) {
		Prober::error("dlopen failed: %s", dl_error);
	}

	base_func = dlsym(library, BASE_FUNC);
	if ((dl_error = dlerror()) != nullptr)  {
		Prober::error("error in dlsym : %s",dl_error);
	}

	Prober::info("LIB is at      %p",library);
	Prober::info("func_base is at      %p",base_func);

	cout << "instance de client " << endl;
	c = new Client("xen", Prober::SENDER, 250, &library, &base_func);

	cout << *c << endl;

	c->charToPacket(buffer);

	vector<Packet> p = c->getStackPacket();

	cout << "affichage des paquets segmentés" << endl;

	for(const Packet& e : p) {
		cout << e << endl;
	}
	cout << "sending packet" << endl;
	auto t1 = std::chrono::high_resolution_clock::now();
	c->send();
	auto t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::seconds>( t2 - t1 ).count();

	cout << "duration : " <<  duration <<endl;
	cout << "util throughput : " << s.size() / duration << " o/s" << endl;
	cout << "real throughtput:" << p.size()*sizeof(struct packet_s) / duration << " o/s" << endl;
	return 0;
}