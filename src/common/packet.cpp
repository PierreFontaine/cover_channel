//
// Created by pfontaine on 09/10/2019.
//

#include "packet.h"

using namespace std;

BadSizePacket::BadSizePacket(const string &arg) throw() : runtime_error(arg) {}
BadSizePacket::~BadSizePacket() {}

/**
 * @brief A way to set all values to 0
 */
void Packet::init() {
	p._checksum = 0;
	p._length = 0;
	p._id = 0;
	memset(p._data, 0, 64 * sizeof(char));
}

Packet::~Packet() {}

/**
 * @brief default constructor which init all values to 0
 */
Packet::Packet() {
	init();
}

/**
 * @brief copy constructor for coplien form which take one Packet
 * and copy its elements
 * @param p1 a Packet instance
 */
Packet::Packet(const Packet &p1) {
	init();
	memcpy(&p, &p1.p, sizeof(struct packet_s));
}

/**
 * @brief constructor which init a Packet instance with a packet
 * structure.
 * @note Packet class and packet structure are 2 differents entities
 * @param p1
 */
Packet::Packet(const packet &p1) {
	init();
	memcpy(&p, &p1, sizeof(struct packet_s));
}

/**
 * @brief return a packet structure
 * @return a packet
 */
packet Packet::getPacket() const {
	return p;
}

/**
 * @brief return the id of the packet
 * @return an id
 */
int Packet::getId() const {
	return p._id;
}

/**
 * @brief return the data length of a packet
 * @return a length
 */
int Packet::getLength() const {
	return p._length;
}

/**
 * @brief return the checksum of a packet
 * @return a checksum
 */
int Packet::getChecksum() const {
	return p._checksum;
}

/**
 * @brief return the data
 * @return a string with data values
 */
string Packet::getData() const {
	return string(p._data);
}

/**
 * @brief allow to inject data in packet
 * @param c a string to pass
 */
void Packet::setData(char *c) {
	string s = string(c);
	if (s.length() > 64) {
		cerr << "string too large" << endl;
	} else {
		memcpy(&p._data, c, s.length()*sizeof(char));
	}
}

/**
 * @brief allow to dynamically change the checksum
 * should not be use, only for tests purpose
 * @deprecated
 * @param c
 */
void Packet::setChecksum(int c) {
	p._checksum = c;
}

/**
 * @brief calcul the checksum of the packet and compare
 * the result with stored checksum.
 * @return true is packet is correct
 */
bool Packet::isPacketCorrect() {
	size_t const data_len = 64;

	boost::crc_basic<16>  crc_ccitt1( 0x1021, 0xFFFF, 0, false, false );
	crc_ccitt1.process_bytes( p._data, data_len );

	return p._checksum == crc_ccitt1.checksum();
}

/**
 * @brief naive checksum
 * @note do not prevent all the errors, typically
 * this kind of checksum allow ab == ba
 * that's why we should introduce the idea of polynome
 */
void Packet::calculChecksum() {
	size_t const data_len = 64;

	boost::crc_basic<16>  crc_ccitt1( 0x1021, 0xFFFF, 0, false, false );
	crc_ccitt1.process_bytes( p._data, data_len );
	p._checksum = crc_ccitt1.checksum();
}

/**
 * @brief Serialize a Structure into a packet so it will be easier to
 * be sended through our caches as 1 and 0 !
 * @return a pointer on our array
 */
char* Packet::serializePacket(char* arr) const {

	memcpy(arr, &p, sizeof(struct packet_s));
	return arr;
}

/**
 * @brief Transform a vector which contain 0 and 1 to a packet
 * @param v a vector of int {0,1}
 * @return a packet
 */
packet Packet::vectToPacket(vector<int> v) {
	auto res_arr = new unsigned char[sizeof(struct packet_s)];
	packet res = {0,0,0,0};

	for(int i = 0; i < sizeof(struct packet_s); i++) {
		//Create a subvector
		vector<int>::const_iterator sub_begin = v.begin() + (i * 8);
		vector<int>::const_iterator sub_end = v.begin() + (((i + 1) * 8) - 1);

		vector<int> sub(sub_begin, sub_end);
		res_arr[i] = 0x00;
		bitset<8> b = 0x00;
		for (unsigned int j = 0; j < 8; j++) {
			b[j] = sub[j];
		}
		unsigned long v = b.to_ulong();
		res_arr[i] = static_cast<char>(v);
	}
	memcpy(&res, res_arr, sizeof(struct packet_s));
	return  res;
}