//
// Created by pfontaine on 09/10/2019.
//

#ifndef QUICK_C_PACKET_H
#define QUICK_C_PACKET_H

#include <vector>
#include <cstring>
#include <iostream>
#include <sstream>
#include <boost/crc.hpp>
#include "stdio.h"
#include <bitset>

using namespace std;

/**
 * @brief Throwable exception when attempting to create a packet while
 * it'll doesn't fit the structure
 */
class BadSizePacket : runtime_error {
	public:
		BadSizePacket(const string &arg) throw();
		virtual ~BadSizePacket() throw();
};

typedef struct packet_s{
	int _id;
	int _length;
	int _checksum;
	char _data[64];
}packet;

class Packet {
	packet p;
	public:
		Packet();
		Packet(const Packet&);
		Packet(const packet &);
		~Packet();

		/**GETTER**/
		packet getPacket() const;
		int getId() const;
		int getLength() const;
		int getChecksum() const;
		string getData() const;

		/**SETTER**/
		void setData(char*);
		void setChecksum(int); //Only for tests usage

		/**MISC**/
		bool isPacketCorrect();
		void calculChecksum();
		char* serializePacket(char* arr) const;

		static packet vectToPacket(vector<int> v);
	private:
		void init();
};

/**
 * @brief Override the out stream operator
 * @param out
 * @param p1 instance of Packet
 * @return out stream which will contain our information
 */
inline ostream &operator <<(ostream &out, const Packet& p1) {
	out << "packet information\n" <<
	    "[-] id " << p1.getId() << "\n" <<
	    "[-] length " << p1.getLength() << "\n" <<
	    "[-] checksum " << p1.getChecksum() << "\n" <<
	    "[-] data " << p1.getData() << "\n" << endl;

	return out;
}

#endif //QUICK_C_PACKET_H
