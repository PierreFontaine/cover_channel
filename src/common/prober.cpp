//
// Created by pfontaine on 26/09/19.
//

#include "prober.h"

#include <algorithm>

using namespace std;

Prober::Prober(){
	_m = UNDEFINED;
	_timeout = 120;
}

/**
 * @brief Constructeur de la classe Prober
 */
Prober::Prober(Prober::mode m, int timeout) {
	_m = m;
	_timeout = timeout;
}

/**
 * @brief Destructeur de la classe Prober
 */
Prober::~Prober() {}

/**
 * @brief
 * @return
 */
int Prober::probe(void* addr) {
	int res = 0;
	switch (_m) {
		case FLUSH_FLUSH : {
			res = probe_ff(addr);
			break;
		}
		case FLUSH_RELOAD : {
			res = probe_fr(addr);
			break;
		}
		case SENDER : {
			load(addr);
		}
		default: break;
	}
	return res;
}

/**
 * @brief
 * @return
 */
int Prober::probe_fr(void *addr) {
	volatile unsigned long time;
	asm __volatile__ (
	" mfence \n"
	" lfence \n"
	" rdtsc \n"
	" lfence \n"
	" movl %%eax, %%esi \n"
	" movl (%1), %%eax \n"
	" lfence \n"
	" rdtsc \n"
	" subl %%esi, %%eax \n"
	" lfence \n"
	" clflush 0(%1) \n"
	: "=a" (time)
	: "c" (addr)
	: "%esi", "%edx");
	if ( time < 120 ) {
		return 1;
	}
	return 0;
}

/**
 * @brief
 * @return
 */
int Prober::probe_ff(void *addr) {
	volatile unsigned long time;
	asm __volatile__ (
	" mfence \n"
	" lfence \n"
	" rdtsc \n"
	" lfence \n"
	" movl %%eax, %%esi \n"
	" clflush 0(%1) \n"
	" lfence \n"
	" rdtsc \n"
	" subl %%esi, %%eax \n"
	" clflush 0(%1) \n"
	: "=a" (time)
	: "c" (addr)
	: "%esi", "%edx");
	if ( time < 130 ) {
		return 1;
	}
	return 0;
}

/**
 * @brief
 * @param addr
 */
void Prober::load(void *addr) {
	volatile unsigned long time;
	asm __volatile__ (
	" mfence \n"
	" lfence \n"
	" movl %%eax, %%esi \n"
	" movl (%1), %%eax \n"
	: "=a" (time)
	: "c" (addr)
	: "%esi", "%edx");
}


/**
 * @brief Getter for timeout
 * @return
 */
int Prober::getTimeout() const {
	return _timeout;
}

/**
 * @brief Getter for mode
 * @return
 */
Prober::mode Prober::getMode() const {
	return _m;
}

/**
 * @brief Setter for mode variable
 * @param m
 */
void Prober::setMode(Prober::mode m) {
	_m = m;
}

/**
 * @brief Setter for timeout variable
 * @param t
 */
void Prober::setTimeout(int t) {
	_timeout = t;
}

void Prober::info(const char * format, ...) {
	va_list myargs;
	va_start(myargs, format);
	printf("[\033[34;1m-\033[0m] ");
	vprintf(format, myargs);
	printf("\n");
}

void Prober::error(const char * format, ...) {
	va_list myargs;
	va_start(myargs, format);
	printf("[\033[31;1m!\033[0m] ");
	vprintf(format, myargs);
	printf("\n");
	exit(1);
}