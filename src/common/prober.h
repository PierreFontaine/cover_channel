//
// Created by pfontaine on 26/09/19.
//


#ifndef SRC_PROBER_H
#define SRC_PROBER_H

#include <tiff.h>
#include <dlfcn.h>
#include <iostream>
#include "stdio.h"
#include <stdarg.h>

using namespace std;

class Prober {
public:
	enum mode {UNDEFINED, FLUSH_FLUSH, FLUSH_RELOAD, SENDER};
private:
	mode _m;
	int _timeout;

	public:
		Prober();
		Prober(Prober::mode m, int timeout);
		~Prober();

		int probe(void *addr);
		/** GETTER **/
		int getTimeout() const;
		mode getMode() const;

		/** SETTER **/
		void setMode(mode m);
		void setTimeout(int t);

		/** MISC **/
		static void info(const char* format, ...);
		static void error(const char* format, ...);
	private:
		int probe_fr(void *addr);
		int probe_ff(void *addr);
		void load(void *addr);
};

#endif //SRC_PROBER_H
